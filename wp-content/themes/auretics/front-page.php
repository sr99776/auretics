<?php
/*
 Template Name: Home Page Template
  */
get_header();
?>
<section class="banner">
<div class="blank_div"> 
</div>
  <div class="container">
    <video playsinline="" id="my-video" width="100%" height="100%" loop="" muted="" autoplay="autoplay" loading="lazy">
      <source src=" <?php echo get_template_directory_uri(); ?>/assets/images/video/video.mp4" type="video/mp4">
    </video>
    <div class="row">
        <div class="col-md-8 col-8 col-sm-8 banner-video-text">
          <div class="heading-business home-business">
            <h1>India’s <span>fastest</span> growing
              <span><br>Direct Selling</span> Company
            </h1>
            <p>To pursue their passion for your brighter tomorrow !</p>
            <a href="">Explore Now</a>
          </div>
        </div>
        <div class="col-md-4 col-4 col-sm-4 banner-video-text">
          <div class="banner-logo">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/site-logo.svg');" alt="site-logo">
          </div>
        </div>
    </div>
  </div>
</section>

<?php if (have_rows('marque_logo_brand')) : ?>
  <section class="marquee-line">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="marque-slider">
            <?php while (have_rows('marque_logo_brand')) : the_row(); ?>
              <div>
                <ul>
                  <li>
                    <span>
                      <img src="<?php echo get_sub_field('image') ?>" alt="marque">
                    </span><?php echo get_sub_field('text') ?>
                  </li>
                </ul>
              </div>
            <?php endwhile; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>


<?php if (have_rows('growth_section')) : ?>
  <?php while (have_rows('growth_section')) : the_row(); ?>
    <section class="growth">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-10">
            <div class="row">
              <div class="col-12">
                <div class="growth-heading">
                  <p class="orange-color"><?php echo get_sub_field('title') ?></p>
                  <h2 class="pb-4"><?php echo get_sub_field('heading') ?></h2>
                </div>
              </div>
            </div>
            <?php if (have_rows('content_box')) : ?>
              <?php while (have_rows('content_box')) : the_row(); ?>
                <div class="row no-gutter background-color">
                  <div class="col-md-6 boss">
                    <img src="<?php echo get_sub_field('image') ?>" alt="growth" class="img-fluid">
                  </div>
                  <div class="col-md-6">
                    <div class="boss-content">
                      <h3><?php echo get_sub_field('content_box_heading') ?></h3>
                      <p><?php echo get_sub_field('content') ?></p>
                    </div>
                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('about_auretics')) : ?>
  <?php while (have_rows('about_auretics')) : the_row(); ?>
    <section class="auretics-limited" style="background-image: url('<?php echo get_sub_field('background_image') ?>');">
      <div class="container">
        <div class="row">
          <?php if (have_rows('left_side_content')) : ?>
            <?php while (have_rows('left_side_content')) : the_row(); ?>
              <div class="col-md-12 col-lg-12 col-xl-6">
                <div class="limited-wrapper">
                  <img src="<?php echo get_sub_field('about_image') ?>" alt="limited-background" class="img-fluid">
                  <div class="fastest d-flex">
                    <div> <img src="<?php echo get_sub_field('icon_image') ?>" alt="ribbon"></div>
                    <div class="fast-content">

                      <h3><?php echo get_sub_field('heading') ?></h3>
                      <p><?php echo get_sub_field('text') ?></p>
                    </div>
                  </div>
                </div>

              </div>
            <?php endwhile; ?>
          <?php endif; ?>

          <?php if (have_rows('right_side_content')) : ?>
            <?php while (have_rows('right_side_content')) : the_row(); ?>
              <div class="col-md-12 col-lg-12 col-xl-6">
                <div class="right-fast-content">
                  <h2 class="pb-3"><?php echo get_sub_field('about_us_heading') ?></h2>
                  <p><?php echo get_sub_field('content') ?></p>
                  <?php
                  $link = get_sub_field('link');
                  if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                  else :
                    $link_url = '#';
                  endif;
                  ?>
                  <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
                </div>

              </div>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>

    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('product_category_section')) : ?>
  <?php while (have_rows('product_category_section')) : the_row(); ?>
    <section class="product-categories">
      <div class="container">
        <div class="row">
          <div class="range">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>

          <div class="discover-category-slider">
            <?php if (have_rows('category_box')) : ?>
              <?php while (have_rows('category_box')) : the_row(); ?>
                <div class="col-md-3">
                  <div class="discover-category">
                    <img src="<?php echo get_sub_field('category_image') ?>" alt="category" class="img-fluid">
                    <h6><?php echo get_sub_field('category_title') ?></h6>
                    <p><?php echo get_sub_field('category_content') ?></p>
                    <?php
                    $link = get_sub_field('link');
                    if ($link) :
                      $link_url = $link['url'];
                      $link_title = $link['title'];
                      $link_target = $link['target'] ? $link['target'] : '_self';
                    else :
                      $link_url = '#';
                    endif;
                    ?>
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>

        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('products_tab')) : ?>
  <?php while (have_rows('products_tab')) : the_row(); ?>
    <section class="products">
      <div class="container">
        <div class="row">
          <div class="range-2">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="product-slider">

            <?php if (have_rows('products')) : ?>
              <?php while (have_rows('products')) : the_row(); ?>
                <div class="col-md-3 product-border">
                  <?php
                  $link = get_sub_field('link');
                  if ($link) :
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                  else :
                    $link_url = '#';
                  endif;
                  ?>
                  <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>">

                    <img src="<?php echo get_sub_field('product_image') ?>" alt="Auretics Shower Gel">
                    <div class="products-data text-center">
                      <div class="rating-icons">
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                      </div>
                      <span><?php echo get_sub_field('product_title') ?></span>
                      <p><?php echo get_sub_field('product_price') ?></p>
                    </div>
                  </a>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('certification_section')) : ?>
  <?php while (have_rows('certification_section')) : the_row(); ?>
    <section class="certification">
      <div class="container">
        <div class="row">
          <div class="range-3 pb-3">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-3"><?php echo get_sub_field('heading') ?></h2>
          </div>

          <?php if (have_rows('iso_images')) : ?>
            <?php while (have_rows('iso_images')) : the_row(); ?>
              <div class="col-md-4">
                <div class="certificate-image">
                  <img src="<?php echo get_sub_field('image') ?>" alt="certificate-1">
                </div>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>
    </section>
  <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('rewards_recognition')) : ?>
  <?php while (have_rows('rewards_recognition')) : the_row(); ?>
    <section class="product-categories">
      <div class="container">
        <div class="row">
          <div class="range">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="category-slider">
            <?php if (have_rows('rewards_box')) : ?>
              <?php while (have_rows('rewards_box')) : the_row(); ?>
                <div class="col-md-4">
                  <div class="discover-category">
                    <img src="<?php echo get_sub_field('rewards_image') ?>" alt="category">
                    <h6><?php echo get_sub_field('rewards_title') ?></h6>
                    <p><?php echo get_sub_field('rewards_content') ?></p>
                    <?php
                    $link = get_sub_field('link');
                    if ($link) :
                      $link_url = $link['url'];
                      $link_title = $link['title'];
                      $link_target = $link['target'] ? $link['target'] : '_self';
                    else :
                      $link_url = '#';
                    endif;
                    ?>
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo esc_attr($link_title); ?></a>
                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('testimonil_section_home')) : ?>
  <?php while (have_rows('testimonil_section_home')) : the_row(); ?>
    <section class="testimonial">
      <div class="container">
        <div class="row">
          <div class="testi-one">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="home-testimonial-slider">
            <?php if (have_rows('testimonial')) : ?>
              <?php while (have_rows('testimonial')) : the_row(); ?>
                <div class="col-md-4">
                  <div class="testimonial-one" style="width: 100%; display: inline-block;">
                    <div class="img">
                      <img src="http://auretics.test/wp-content/themes/auretics/assets/images/icon/arrow.svg');" alt="arrow">
                      <h3><?php echo get_sub_field('heading') ?></h3>
                      <p class="join-para"><?php echo get_sub_field('review') ?></p>
                      <div class="author">
                        <p>
                          <span><?php echo get_sub_field('reviewer_name') ?></span>

                          <span><?php echo get_sub_field('reviewer__title') ?></span>
                        </p>
                        <span>
                          <img src="<?php echo get_sub_field('image') ?>" class="img-fluid" alt="Mr. Vikas Kumar Dey">
                        </span>
                      </div>
                    </div>

                  </div>
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
            <!-- <div class="col-md-4">
              <div class="testimonial-one" style="width: 100%; display: inline-block;">
                <div class="img">
                  <img src="http://auretics.test/wp-content/themes/auretics/assets/images/icon/arrow.svg');" alt="arrow">
                  <h3><span>Unbelievable Experience!!</span></h3>
                  <p class="join-para">No other Direct Selling Company has spray based product like "SugarRodhi" for sugar management. That's making it peculiar. Also, the customer care service is instantly available to the customers which is very helpful.</p>
                  <div class="author">
                    <p>
                      <span>Mr. Vikas Kumar Dey</span>
                      <br>
                      <span>Independent Auretics Distributor</span>
                    </p>
                    <span>
                      <img src="http://auretics.test/wp-content/themes/auretics/assets/images/test-1.webp');" class="img-fluid" alt="Mr. Vikas Kumar Dey">
                    </span>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-md-4">
              <div class="testimonial-one" style="width: 100%; display: inline-block;">
                <div class="img">
                  <img src="http://auretics.test/wp-content/themes/auretics/assets/images/icon/arrow.svg');" alt="arrow">
                  <h3><span>Unbelievable Experience!!</span></h3>
                  <p class="join-para">No other Direct Selling Company has spray based product like "SugarRodhi" for sugar management. That's making it peculiar. Also, the customer care service is instantly available to the customers which is very helpful.</p>
                  <div class="author">
                    <p>
                      <span>Mr. Vikas Kumar Dey</span>
                      <br>
                      <span>Independent Auretics Distributor</span>
                    </p>
                    <span>
                      <img src="http://auretics.test/wp-content/themes/auretics/assets/images/test-1.webp');" class="img-fluid" alt="Mr. Vikas Kumar Dey">
                    </span>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-md-4">
              <div class="testimonial-one" style="width: 100%; display: inline-block;">
                <div class="img">
                  <img src="http://auretics.test/wp-content/themes/auretics/assets/images/icon/arrow.svg');" alt="arrow">
                  <h3><span>Unbelievable Experience!!</span></h3>
                  <p class="join-para">No other Direct Selling Company has spray based product like "SugarRodhi" for sugar management. That's making it peculiar. Also, the customer care service is instantly available to the customers which is very helpful.</p>
                  <div class="author">
                    <p>
                      <span>Mr. Vikas Kumar Dey</span>
                      <br>
                      <span>Independent Auretics Distributor</span>
                    </p>
                    <span>
                      <img src="http://auretics.test/wp-content/themes/auretics/assets/images/test-1.webp');" class="img-fluid" alt="Mr. Vikas Kumar Dey">
                    </span>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-md-4">
              <div class="testimonial-one" style="width: 100%; display: inline-block;">
                <div class="img">
                  <img src="http://auretics.test/wp-content/themes/auretics/assets/images/icon/arrow.svg');" alt="arrow">
                  <h3><span>Unbelievable Experience!!</span></h3>
                  <p class="join-para">No other Direct Selling Company has spray based product like "SugarRodhi" for sugar management. That's making it peculiar. Also, the customer care service is instantly available to the customers which is very helpful.</p>
                  <div class="author">
                    <p>
                      <span>Mr. Vikas Kumar Dey</span>
                      <br>
                      <span>Independent Auretics Distributor</span>
                    </p>
                    <span>
                      <img src="http://auretics.test/wp-content/themes/auretics/assets/images/test-1.webp');" class="img-fluid" alt="Mr. Vikas Kumar Dey">
                    </span>
                  </div>
                </div>

              </div>
            </div> -->
          </div>
        </div>
      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>

<section class="capitative">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div class="everywhere">
          <h2>How Auretics is Captivating<br>
            Audiences Everywhere</h2>
        </div>
      </div>
      <div class="col-md-5">
        <div class="grabbed">
          <p>successfully grabbed the attention of people across various platforms and channels. Its likely to have a large following and a loyal customer base due to its ability to create content and marketing strategies that are both compelling and memorable.</p>
        </div>
      </div>
    </div>

    <div class="row stories">
      <div class="col-md-3 col-12">
        <div class="image-box">
          <a href="" class="award-video">
            <img src="http://auretics.test/wp-content/themes/auretics/assets/images/audience-1.png');" class="img-fluid" alt="audience">
          </a>
          <p>Auretics Limited uncaps the benefits of direct selling through its Auretics Super Infinity Plan – ThePrint</p>
          <div class="explore-button">
            <a href="">Explore Now</a>
          </div>
        </div>

        <div class="image-box">
          <a href="" class="award-video">
            <img src="http://auretics.test/wp-content/themes/auretics/assets/images/audience-5.png');" class="img-fluid" alt="audience">
          </a>
          <p>The benefits of direct selling with Auretics Super Infinity Plan - Hindustan Times</p>
          <div class="explore-button">
            <a href="">Explore Now</a>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-12 d-flex align-items-center">
        <div class="image-box">
          <a href="" class="award-video">
            <img src="http://auretics.test/wp-content/themes/auretics/assets/images/audience-3.png');" class="img-fluid" alt="audience">
          </a>
          <p>Auretics Limited uncaps the benefits of direct selling through its Auretics Super Infinity Plan – ThePrint</p>
          <div class="explore-button">
            <a href="">Explore Now</a>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-12">
        <div class="image-box">
          <a href="#" class="award-video">
            <img src="http://auretics.test/wp-content/themes/auretics/assets/images/audience-2.png');" class="img-fluid" alt="audience">
          </a>
          <p>Munnabhai achieved the highest qualification title at Auretics - Hindustan Times</p>
          <div class="explore-button">
            <a href="">Explore Now</a>
          </div>
        </div>

        <div class="image-box">
          <a href="" class="award-video">
            <img src="http://auretics.test/wp-content/themes/auretics/assets/images/audience-1.png');" class="img-fluid" alt="audience">
          </a>
          <p>Auretics Limited uncaps the benefits of direct selling through its Auretics Super Infinity Plan – ThePrint</p>
          <div class="explore-button">
            <a href="">Explore Now</a>
          </div>
        </div>
      </div>

    </div>

  </div>
</section>

<?php if (have_rows('8_things_know')) : ?>
  <?php while (have_rows('8_things_know')) : the_row(); ?>
    <section class="eight-things" style="background-image: url('<?php echo get_sub_field('background_image') ?>');">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="eight-things-wrapper">
              <p class="white-wrap"><?php echo get_sub_field('title') ?></p>
              <h2 class="white-wrap"><?php echo get_sub_field('heading') ?></h2>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="row mt-5 desktop-content">
              <?php $i = 1; ?>
              <?php if (have_rows('steps')) : ?>
                <div class="col-12">
                  <div class="ct-image-box text-center">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/group-circle.png');" alt="things">
                    <?php while (have_rows('steps')) : the_row(); ?>
                      <div class="ct-list-item" id="number<?php echo $i; ?>">
                        <div class="ct-list-number">0<?php echo $i; ?></div>
                        <div class="ct-list-content"><?php echo get_sub_field('steps_title') ?></div>

                      </div>
                      <?php $i++; ?>
                    <?php endwhile; ?>
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <div class="row">
              <div class="mob-thing-img">
                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/group-circle.png');" alt="things">
              </div>
              <div class="col-md-6 mobile-content">
                <div class="ct-list-item">
                  <div class="ct-list-number">01</div>
                  <div class="ct-list-content"> Unlimited Income Potential</div>

                </div>

                <div class="ct-list-item">
                  <div class="ct-list-number">02</div>
                  <div class="ct-list-content"> ISO Certified & registered by Govt of India</div>

                </div>
                <div class="ct-list-item">
                  <div class="ct-list-number">03</div>
                  <div class="ct-list-content"> Personality Development Seminars by Industry Experts</div>

                </div>
                <div class="ct-list-item">
                  <div class="ct-list-number">04</div>
                  <div class="ct-list-content">Made-In-India Products Across PAN India</div>

                </div>
              </div>
              <div class="col-md-6 mobile-content pt-0">
                <div class="ct-list-item list-side">
                  <div class="ct-list-number">05</div>
                  <div class="ct-list-content">Flexibility and Freedom</div>

                </div>
                <div class="ct-list-item list-side">
                  <div class="ct-list-number">06</div>
                  <div class="ct-list-content"> Counselling Sessions for Personal Growth</div>

                </div>
                <div class="ct-list-item list-side">
                  <div class="ct-list-number">07</div>
                  <div class="ct-list-content">100% Customer Satisfaction Guaranteed</div>

                </div>
                <div class="ct-list-item list-side">
                  <div class="ct-list-number">08</div>
                  <div class="ct-list-content"> Right Product at Right Price</div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      </div>
    </section>
  <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('coverage_media')) : ?>
  <?php while (have_rows('coverage_media')) : the_row(); ?>
    <section class="news-press">
      <div class="container">
        <div class="row">
          <div class="press">
            <p class="orange-color"><?php echo get_sub_field('title') ?></p>
            <h2 class="text-center pb-5"><?php echo get_sub_field('heading') ?></h2>
          </div>
          <div class="row justify-content-center align-items-center">
            <?php if (have_rows('news_image')) : ?>
              <?php while (have_rows('news_image')) : the_row(); ?>
                <div class="col-md-2 col-6">
                  <img src="<?php echo get_sub_field('image') ?>" class="img-fluid" alt="audience">
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
            <!--  <div class="col-md-2 col-6">
              ass="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-3.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-4.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-5.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-6.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-8.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-7.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-9.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-10.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-10.png');" class="img-fluid" alt="audience">

            </div>
            <div class="col-md-2 col-6">
              <img src="http://auretics.test/wp-content/themes/auretics/assets/images/news-10.png');" class="img-fluid" alt="audience">

            </div> -->
          </div>


        </div>
      </div>


    </section>

  <?php endwhile; ?>
<?php endif; ?>


<?php
// Banner Section
get_template_part('template-parts/join-today-section');
?>


<?php
// Banner Section
get_template_part('template-parts/contact-form');
?>






<?php
get_footer();
?>