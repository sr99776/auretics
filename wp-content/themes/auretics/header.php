<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">

<head>
    <meta>
    <meta <?php bloginfo('charset'); ?>>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/assets/img/icons/favicon.webp" type="image/webp" />
    <title><?php echo get_the_title(); ?></title>
    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-2 col-sm-12">
                    <div class="nav-data d-flex">
                        <button class="navbar-toggler" id="open-btn" type="button" data-toggle="collapse" data-target="#" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon">
                                    <i class="fas fa-bars"></i>
                                    </span>
                                </button>

                                <button class="navbar-toggler" id="close-btn" type="button" data-toggle="collapse" data-target="#" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon close-btn-icon">
                                    <i class="fas fa-close"></i>
                                    </span>
                                </button>
                        <a class="navbar-brand" href="<?php echo get_site_url();?>"><img src="<?php echo get_field('logo','option'); ?>" alt="logo"></a>

                    </div>
                </div>
                <div class="col-md-12 col-lg-10 col-sm-12">
                    <!-- <div class="navbar-collapse collapse" id="navbarSupportedContent">
                        <ul class="d-flex nav-menu">
                            <li><a href="">Home</a></li>
                            <li><a href="">Products</a></li>
                            <li><a href="">Login</a></li>
                            <li><a href="">About Us</a></li>
                            <li><a href="">Press Release</a></li>
                            <li><a href="">Contact Us</a></li>
                            <li class="become-member"><a href="">Become A Member</a></li>
                        </ul> 
                    </div> -->
                    <?php
                        // Header Menu
                        if (function_exists('register_primary_menu')) :
                            wp_nav_menu(array(
                                'theme_location' => 'Header',
                                'menu_class' => 'd-flex nav-menu',
                                'container_id' => 'navbarSupportedContent',
                                'container_class' => 'collapse navbar-collapse',
                                // 'add_li_class'  => 'nav-item',
                                // 'add_a_class'  => 'nav-link',
                            ));
                        endif;
                    ?>
                </div>
            </div>
        </div>

    </header>