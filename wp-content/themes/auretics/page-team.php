<?php
/*
 Template Name: Team Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php if(have_rows('introduction_section')): ?>
    <?php while(have_rows('introduction_section')): the_row();?>
        <section class="procedure management-team">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="focus-team text-center">
                            <span class="explore"><?php echo get_sub_field('title'); ?></span>
                            <h2 class="pt-4"><?php echo get_sub_field('heading'); ?></h2>
                            <p><?php echo get_sub_field('content'); ?></p>
                        </div>
                    </div>
                
                </div>
            </div>
        </section>
    <?php endwhile;?>
<?php endif; ?>

<?php if(have_rows('chief_executive_officer')): ?>
    <section class="execuitive-officer">
        <div class="container">
            <?php while(have_rows('chief_executive_officer')): the_row();?>
                <div class="row">
                <?php if(have_rows('left_side_content')): ?>
                    <?php while(have_rows('left_side_content')): the_row();?>
                        <div class="col-md-12 col-lg-5">
                            <div class="arjun-img">
                                <img src="<?php echo get_sub_field('image'); ?>" alt="Arjun Gupta">
                            </div>
                            <div class="arjun-views">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/comma.png');" alt="Arjun Gupta">
                                <p><?php echo get_sub_field('image_title'); ?></p>
                            </div>
                        </div>
                    <?php endwhile;?>
                <?php endif; ?>
                <?php if(have_rows('right_side_content')): ?>
                    <?php while(have_rows('right_side_content')): the_row();?>
                        <div class="col-md-12 col-lg-7">
                            <div class="officer-team">
                                <p class="explore team-padding"><?php echo get_sub_field('title'); ?></p>
                                <h3><?php echo get_sub_field('heading'); ?></h3>
                                <?php if(have_rows('social_icon')): ?>
                                    <div class="social-team">
                                            <?php while(have_rows('social_icon')): the_row();?>
                                            <?php
                                                $link = get_sub_field('link');
                                                if ($link) :
                                                    $link_url = $link['url'];
                                                    $link_title = $link['title'];
                                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                                else :
                                                    $link_url = '#';
                                                endif;
                                            ?>
                                            <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><?php echo get_sub_field('icon'); ?></a>
                                            
                                            
                                            <?php endwhile;?>
                                        </div>
                                <?php endif; ?>
                                <div class="arjun-font">
                                    <?php echo get_sub_field('content'); ?>
                                </div>
                            
                            </div>
                        </div>
                    <?php endwhile;?>
                <?php endif; ?>
                </div>
            <?php endwhile;?>
        </div>
    </section>
<?php endif; ?>


<?php if(have_rows('team_managment')): ?>
    <?php while(have_rows('team_managment')): the_row();?>
        <section class="organization pt-0  procedure management-team">
            <div class="container">
                <div class="row text-center pb-5">
                    <div class="col-md-12">
                        <span class="explore pb-3"><?php echo get_sub_field('title'); ?></span>
                        <h2 class="pt-2"><?php echo get_sub_field('heading'); ?></h2>
                                                        
                        <p><?php echo get_sub_field('content'); ?></p>
                    </div>
                </div>
                
                <?php if(have_rows('team_box')): ?>
                    <div class="row on-desktop">
                            <?php while(have_rows('team_box')): the_row();?>
                                <div class="col-md-3">
                                    <div class="organize-img">
                                        <img src="<?php echo get_sub_field('image'); ?>" alt="Mr. Arvind Rao">
                                    </div>
                                    <div class="wrapper-team">
                                        <div class="organize-wrap">
                                            <h6><?php echo get_sub_field('name'); ?></h6>
                                            <p><?php echo get_sub_field('title'); ?></p>
                                        </div>
                                    </div>
                                </div>  
                                
                            <?php endwhile;?>
                        </div>
                <?php endif; ?>
                
                <?php if(have_rows('team_box')): ?>
                    <div class="row pt-0 on-mobile">
                        <div class="team-slider">
                            <?php while(have_rows('team_box')): the_row();?>
                                <div class="col-md-3">
                                    <div class="organize-img">
                                        <img src="<?php echo get_sub_field('image'); ?>" alt="Mr. Arvind Rao">
                                    </div>
                                    <div class="wrapper-team">
                                        <div class="organize-wrap">
                                            <h6><?php echo get_sub_field('name'); ?></h6>
                                            <p><?php echo get_sub_field('title'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
        <?php endwhile;?>
<?php endif; ?>


<section class="organization ">
    <div class="container">
        
    </div>
</section>

























<?php
get_footer();
?>