<?php
/*
 Template Name: Business  Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php if (have_rows('introduction_section')) : ?>
    <?php while (have_rows('introduction_section')) : the_row(); ?>
        <section class="opportunity">
            <div class="container">
                <div class="row">
                    <div class="text-center">
                        <p class="explore"><?php echo get_sub_field('title'); ?></p>
                        <h2><?php echo get_sub_field('heading'); ?></h2>
                        <p class="para"><?php echo get_sub_field('content'); ?></p>
                        <?php
                        $link = get_sub_field('link');
                        if ($link) :
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                        else :
                            $link_url = '#';
                        endif;
                        ?>
                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" title="" class="read-btn"> <button><?php echo esc_attr($link_title); ?></button> </a>
                        <!-- <a href=""></a> -->
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php if (have_rows('ways_of_income_section')) : ?>
    <?php while (have_rows('ways_of_income_section')) : the_row(); ?>
        <section class="compensation" style="background-image: url('<?php echo get_sub_field('background_image'); ?>');">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="all-compensation">
                            <p class="explore"><?php echo get_sub_field('title'); ?></p>
                            <h2 style="color:#FFf;"><?php echo get_sub_field('heading'); ?></h2>
                        </div>
                    </div>
                    <div class="col-md-7 content-box">
                        <?php echo get_sub_field('content'); ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>


<?php if (have_rows('steps_section')) : ?>
    <?php while (have_rows('steps_section')) : the_row(); ?>
        <section class="sucessful">
            <div class="container">
                <div class="success-data text-center">
                    <p class="explore"><?php echo get_sub_field('title'); ?></p>
                    <h2><?php echo get_sub_field('heading'); ?></h2>
                </div>
                <div class="row">
                    <?php if (have_rows('steps')) : ?>
                        <?php while (have_rows('steps')) : the_row(); ?>
                            <div class="col-md-6 steps-box">
                                <?php
                                $link = get_sub_field('link');
                                if ($link) :
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                else :
                                    $link_url = '#';
                                endif;
                                ?>
                                <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_url($link_target); ?>"><button class="button mb-4"><?php echo esc_attr($link_title); ?></button></a>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>


                </div>
                <!-- <div class="row">
                
                </div> -->
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>



<!-- <?php if (have_rows('explore_with_us_section')) : ?>
    <?php while (have_rows('explore_with_us_section')) : the_row(); ?>
        <section class="opportunity-option" style="background-image: url('<?php echo get_sub_field('background_image'); ?>');">
            <div class="container">
                <div class="text-center">
                    <p class="explore"><?php echo get_sub_field('title'); ?></p>
                    <h2><?php echo get_sub_field('heading'); ?></h2>
                    <p class="pb-3"><?php echo get_sub_field('content'); ?></p>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="opportunity-slider">
                            <?php if (have_rows('options_box')) : ?>
                                <?php while (have_rows('options_box')) : the_row(); ?>

                                    <div class="double-box">
                                        <div class="icon-box text-center">
                                            <img src="<?php echo get_sub_field('image'); ?>" alt="tbb" class="img-fluid">
                                        </div>
                                        <div class="four-data-box text-center">
                                            <h3><?php echo get_sub_field('head'); ?></h3>
                                            <p><?php echo get_sub_field('paragraph'); ?></p>
                                        </div>
                                    </div>


                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <?php endwhile; ?>
<?php endif; ?> -->


<?php if (have_rows('apka_apna_business')) : ?>
    <?php while (have_rows('apka_apna_business')) : the_row(); ?>
        <section class="Auretics-apna business">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="text-center">
                            <h2><?php echo get_sub_field('heading'); ?></h2>
                            <p><?php echo get_sub_field('intro_content'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container apna-icon-area pt-5">
                <div class="row   border-on">
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-2.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Savings on Self-Consumption</h6>
                            <p>Upto 30% & 1+1
                                on MRP</p>
                        </div>

                    </div>
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-2.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Super Bonus</h6>
                            <p>1000 BV Matching
                                on Both Organisation(90% Pool)</p>
                        </div>
                    </div>
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-2.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Personal
                                Bonus</h6>
                            <p>2500 BV Matching
                                on both Organisation(10% Pool)</p>
                        </div>
                    </div>
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-4.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Sharing Bonus</h6>
                            <p>10000 BV Matching
                                on Both Organisation(10% Pool)</p>
                        </div>
                    </div>
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-5.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Nurturing & Vacation Bonus</h6>
                            <p>30000 BV Matching
                                on Both Organisation(15% Pool)</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-start flex-row-reverse last ">
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-9.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Team Building & Automobile Bonus</h6>
                            <p>1250000 BV Matching
                                on Both Organisation(5% Pool)</p>
                        </div>
                    </div>
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-8.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Leadership & Shelter Bonus</h6>
                            <p>200000 BV Matching on Both Organisation(10% Pool)</p>
                        </div>
                    </div>
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-7.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Business Master Bonus</h6>
                            <p>125000 BV Matching
                                on Both Organisation(20% Pool)</p>
                        </div>
                    </div>
                    <div class="apna-business-icon">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-6.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Mentorship
                                Bonus</h6>
                            <p>50000 BV Matching
                                on Both Organisation(20% Pool)</p>
                        </div>
                    </div>
                    <!-- <div class="apna-business-icon last">
                        <div class="img">
                            <div class="image-inner">
                                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-6.svg');" class="img-fluid" alt="timeline">
                            </div>
                            <h6>Mentorship
                                Bonus</h6>
                            <p>50000 BV Matching
                                on Both Organisation(20% Pool)</p>
                        </div>
                    </div> -->
                </div>




            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<!-- Mobile -->
<section class="Auretics-apna business mob-apna-business">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="text-center">
                    <h2><span>Auretics</span> Aapka Apna Business</h2>
                    <p class="pb-5">Auretics Compensation Plan ensures that you are earning in proportion to the efforts you put in while being mindful of your earlier efforts and achievements by offering you benefits like rewarding bonuses, pool income, etc.</p>
                </div>
            </div>
        </div>

        <div class="row justify-content-center text-center">
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-2.svg');" class="img-fluid" alt="timeline">
                    <h6>Savings on Self-Consumption</h6>
                    <p>Upto 30% & 1+1
                        on MRP</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-2.svg');" class="img-fluid" alt="timeline">
                    <h6>Super Bonus</h6>
                    <p>1000 BV Matching
                        on Both Organisation(90% Pool)</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-2.svg');" class="img-fluid" alt="timeline">
                    <h6>Personal
                        Bonus</h6>
                    <p>2500 BV Matching
                        on both Organisation(10% Pool)</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-4.svg');" class="img-fluid" alt="timeline">
                    <h6>Sharing Bonus</h6>
                    <p>10000 BV Matching
                        on Both Organisation(10% Pool)</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-5.svg');" class="img-fluid" alt="timeline">
                    <h6>Nurturing & Vacation Bonus</h6>
                    <p>30000 BV Matching
                        on Both Organisation(15% Pool)</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-6.svg');" class="img-fluid" alt="timeline">
                    <h6>Mentorship
                        Bonus</h6>
                    <p>50000 BV Matching
                        on Both Organisation(20% Pool)</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-7.svg');" class="img-fluid" alt="timeline">
                    <h6>Business Master Bonus</h6>
                    <p>125000 BV Matching
                        on Both Organisation(20% Pool)</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-8.svg');" class="img-fluid" alt="timeline">
                    <h6>Leadership & Shelter Bonus</h6>
                    <p>200000 BV Matching on Both Organisation(10% Pool)</p>
                </div>
            </div>
            <div class="col-6">
                <div class="img-time">
                    <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-9.svg');" class="img-fluid" alt="timeline">
                    <h6>Team Building & Automobile Bonus</h6>
                    <p>1250000 BV Matching
                        on Both Organisation(5% Pool)</p>
                </div>
            </div>
        </div>

    </div>



</section>
<!--  -->


<section class="more-rewards">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-12">
                <h2 class="pb-5">And only these, but <span>more rewards !!</span></h2>
            </div>
            <div class="col-3">
                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/timeline-9.svg');" class="img-fluid" alt="timeline">
                <h6>Rocket Income</h6>
                <p>Upto Rs.2,000 extra daily on Rocket Products</p>
            </div>

            <div class="col-3">
                <img src="http://auretics.test/wp-content/themes/auretics/assets/images/lifestyle.svg');" class="img-fluid" alt="timeline">
                <h6>Lifestyle Rewards</h6>
                <p>Earn Rewards for achieving milestones.</p>

            </div>
        </div>
    </div>
</section>



<?php if (have_rows('apka_apna_business')) : ?>
    <?php while (have_rows('apka_apna_business')) : the_row(); ?>
        <section class="Auretics-apna business d-md-block  d-none">
            <div class="container">
                <div class="row">
                    <p><?php echo get_sub_field('content'); ?></p>

                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>







<?php
get_footer();
?>