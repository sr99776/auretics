(function ($) {
  $(document).ready(function () {
    // $('div.accordion-collapse.collapse.show').prev('h3').addClass('faq-active');
    $("div.accordion-collapse.collapse.show")
      .siblings("h3")
      .addClass("active-faq");

    $(".accordion-item h3").click(function () {
      // console.log('clicked');
      $(".accordion-item h3").removeClass("active-faq");
      $(this).addClass("active-faq");
    });

    $(".testimonial-slider").slick({
      autoplay: false,
      autoplaySpeed: 2000,
      speed: 700,
      pauseOnHover: false,
      infinite: true,
      dots: false,
      margin: 0,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: true,
      prevArrow:
        '<button class="slide-arrow prev-arrow"><i class="fa-solid fa-angle-left"></i></button>',
      nextArrow:
        '<button class="slide-arrow next-arrow"><i class="fa-solid fa-angle-right"></i></button>',
      rows: 3,

      // asNavFor: "client-two-row, .client-three-row",
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },

        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            rows: 1,
            slidesPerRow: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    // opportunity-slider
    $(".opportunity-slider").slick({
      infinite: false,
      dots: false,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    // category-slider
    $(".category-slider").slick({
      infinite: false,
      dots: false,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,

      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
          },
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },

        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });


// discover-category

$(".discover-category-slider").slick({
  infinite: false,
  dots: false,
  margin: 20,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,

  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },

    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
      },
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
});

// auretics-slider
$(".auretics-slider").slick({
  infinite: false,
  dots: false,
  margin: 20,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: false,
  rows:2,

  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        rows:2,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        rows:1,
      },
    },

    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        rows:1,
      },
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        rows:1,
      },
    },
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ],
});




    $("#open-btn").click(function () {
      console.log("clicked");
      $("#navbarSupportedContent").css({
        left: "0vw",
        top: "0px",
        transition: "all 0.45s ease",
        "padding-top": "0px",
      });
      $("#open-btn").css({ display: "none" });
      $("#close-btn").css({
        display: "block",
        position: "relative",
        transform: "translate(292px, 10px)",
        "z-index": "999",
      });

      // $('.homepage-main .row.second-header-row').css({'background-color': '#000'});

      // $('.content').css({'transform': 'translate(0, -50%)', 'top' : '50%', 'left' : '0', 'text-align' : 'center', 'max-width': 'unset', 'width' : '50vw'});
    });

    $("#close-btn").click(function () {
      $("#navbarSupportedContent").css({ left: "-100vw" });
      $("#open-btn").css({ display: "block" });
      $("#close-btn").css({ display: "none" });
      $(".homepage-main .row.second-header-row").css({
        "background-color": "#fff",
        transition: "all 0.45s ease",
      });
      // $('.content>img, .subscribe, .footer').css({'display': 'block', 'width': '100%'});
      // $('.content').css({'transform': 'translateX( -50%)', 'top' : '10%', 'left' : '50%', 'text-align' : 'center', 'max-width': 'unset', 'width' : '50vw'});
    });

    // blog

    $(".blog-slider").slick({
      infinite: false,
      dots: true,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
    // carrer-build-slider
    // $("carrer-build-slider").slick({
    //   infinite: false,
    //   dots: true,
    //   margin: 20,

    //   arrows: false,

    //   responsive: [
    //     {
    //       breakpoint: 1024,
    //       settings: {
    //         slidesToShow: 2,
    //         slidesToScroll: 1,
    //       },
    //     },
    //     {
    //       breakpoint: 992,
    //       settings: {
    //         slidesToShow: 2,
    //         slidesToScroll: 1,
    //       },
    //     },
    //     {
    //       breakpoint: 480,
    //       settings: {
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //       },
    //     },
    //     // You can unslick at a given breakpoint now by adding:
    //     // settings: "unslick"
    //     // instead of a settings object
    //   ],
    // });

    $(".team-slider").slick({
      infinite: false,
      dots: true,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    $(".product-slider").slick({
      infinite: false,
      dots: false,
      margin: 20,
      slidesToShow: 4,
      slidesToScroll: 2,
      arrows: true,
      prevArrow:
        '<button class="slide-arrow prev-arrow"><i class="fa-solid fa-angle-left"></i></button>',
      nextArrow:
        '<button class="slide-arrow next-arrow"><i class="fa-solid fa-angle-right"></i></button>',

      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },

        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            dots: true,
            arrows: false,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 767,
          settings: {
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });

    // home-testimonial

    $(".home-testimonial-slider").slick({
      infinite: false,
      dots: false,
      margin: 20,
      slidesToShow: 3,
      slidesToScroll: 2,
      arrows: true,
      prevArrow:
        '<button class="slide-arrow prev-arrow"><i class="fa-solid fa-angle-left"></i></button>',
      nextArrow:
        '<button class="slide-arrow next-arrow"><i class="fa-solid fa-angle-right"></i></button>',

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 992,
          settings: {
            dots: true,
            arrows: false,
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 767,
          settings: {
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    });
    $(".marque-slider").slick({
      slidesToShow: 4,
      dots: false,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 0,
      speed: 2000,
      infinite: true,
      cssEase: "linear",
      pauseOnHover: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });

    $(window).scroll(function () {
      if ($(this).scrollTop() > 120) {
        $(".header").addClass("fixed");
      } else {
        $(".header").removeClass("fixed");
      }
    });
  });
})(jQuery);
