<?php
/*
 Template Name: Contact Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php
// Banner Section
get_template_part('template-parts/contact-form');
?>

<?php if (have_rows('map_location')) : ?>
    <?php while (have_rows('map_location')) : the_row(); ?>
        <input type="hidden" name="lat" id="lat" value="<?php echo get_sub_field('latitude'); ?>">
        <input type="hidden" name="lat" id="long" value="<?php echo get_sub_field('longitude');  ?>">
        <?php if (!empty(get_sub_field('latitude')) && !empty(get_sub_field('longitude'))) : ?>
            <section id="map">
            </section>
        <?php endif; ?>
<?php endwhile;
endif; ?>



<?php if (have_rows('map_location')) : ?>
    <?php while (have_rows('map_location')) : the_row(); ?>
        <input type="hidden" name="lat" id="lat" value="<?php echo get_sub_field('latitude'); ?>">
        <input type="hidden" name="lat" id="long" value="<?php echo get_sub_field('longitude');  ?>">
        <?php if (!empty(get_sub_field('latitude')) && !empty(get_sub_field('longitude'))) : ?>
            <section id="map">
            </section>
        <?php endif; ?>
<?php endwhile;
endif; ?>



<?php
get_footer();
?>