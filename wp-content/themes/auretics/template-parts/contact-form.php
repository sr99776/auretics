<?php if(have_rows('contact_section', 'option')): ?>
    <?php while(have_rows('contact_section', 'option')): the_row(); ?>
        <section class="form-home" style="background-image: url('<?php if(is_page_template('page-contact.php')){ ?><?php echo get_field('contact_section_bg_image'); }else{ ?> <?php echo get_sub_field('background_image'); } ?> );">
            <div class="container"> 
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div>
                            <?php if(have_rows('contact_details')) :?>
                                <ul>
                                    <?php while(have_rows('contact_details')): the_row(); ?>
                                        <li>
                                            <?php echo get_sub_field('svg_code'); ?>
                                            <div class="form-detail-wrap">
                                                <h5><?php echo get_sub_field('label'); ?></h5>
                                                <p><?php echo get_sub_field('information'); ?></p>
                                            </div>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <?php echo do_shortcode(get_sub_field('form_short_code')); ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>