<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>Amazing way of Passive Income!!</span></h3>
        <p>Auretics has an amazing Business plan. I am pleased with Consistent Retailer's Income(CRI). "Facewash is an amazing product" as it gives result within 2-3 months. The pricing looks very reasonable and packaging also looks appealing.</p>
        <div class="author">
            <p>
                <span>Mrs. Shikha Saxena</span>
                <br>
                <span>Independent Auretics Distributor</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-2.webp');" class="img-fluid" alt="Mrs. Shikha Saxena">
            </span>
        </div>
    </div>
</div>

<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>SugarRodhi is just Fab!! </span></h3>
        <p>The product "SugarRodhi" has given tremendous results. Many people are using SugarRodhi where this product is more than 90 percent effective. Infact,"Speedex Joint Support" is also an effective remedy for Joints pain.</p>
        <div class="author">
            <p>
                <span>Mr. Rajesh Yadav</span>
                <br>
                <span>Independent Auretics Distributor</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-3.webp');" class="img-fluid" alt="Mr. Rajesh Yadav">
            </span>
        </div>
    </div>
</div>
<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>Being your own boss like feeling!!</span></h3>
        <p>People are leaving their private jobs for Auretics as it is giving earning opportunity to people. The product "Multiammrit " is very remarkable and showed effective results in people who are using it. Also, I am very much satisfied by the "Customer Care Operations" instant availability.</p>
        <div class="author">
            <p>
                <span>Ms. Pushpa Negi</span>
                <br>
                <span>Independent Auretics Distributor</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-4.webp');" class="img-fluid" alt="Ms. Pushpa Negi">
            </span>
        </div>
    </div>
</div>
<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>Product is Unbelievable!!</span></h3>
        <p>"SugarRodhi is too much effective in controlling Diabetes of the patients. Auretics is providing marvellous schemes to the customers. The benefits of "Consistent Retailer's Income" is leaving people astonished and makes it distinguishable from other companies.</p>
        <div class="author">
            <p>
                <span>Mr. Gurdeep Singh Gill</span>
                <br>
                <span>Business Owner, ABC Start-up</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-5.webp');" class="img-fluid" alt="Mr. Gurdeep Singh Gill">
            </span>
        </div>
    </div>
</div>
<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>100% Healthy products!!</span></h3>
        <p>I am very much happy with the quality of juices as "Sea Buckthorn", "Aloevera" , "Garcinia". Also, the costing is affordable to me as compared with the market prices. No other direct selling Company is giving products at such affordable rates.</p>
        <div class="author">
            <p>
                <span>Mrs. Bromi Panchal</span>
                <br>
                <span>Business Owner, ABC Start-up</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-6.webp');" class="img-fluid" alt="Mrs. Bromi Panchal">
            </span>
        </div>
    </div>
</div>
<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>Auretics is a Super Company!!</span></h3>
        <p>I believe that "SugarRodhi", "BPStabilizer" are backbone to the company. No other company has spray based products as we have here in Auretics.</p>
        <div class="author">
            <p>
                <span>Mr. Dharmendra Kumar Sharma</span>
                <br>
                <span>Business Owner, ABC Start-up</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-7.webp');" class="img-fluid" alt="Mr. Dharmendra Kumar Sharma">
            </span>
        </div>
    </div>
</div>
<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>100% Organic Products!!</span></h3>
        <p>For me, "CareVed Herbal Eyedrop" is like elixir to the eyes. It provides me with the deepest relaxation. Also, I use "Acno-Vanish" Cream and the result is outshown. I am very happy and satisfied with the Pricing and Quality of the products.</p>
        <div class="author">
            <p>
                <span>Mr. Arjun Paramar</span>
                <br>
                <span>Business Owner, ABC Start-up</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-8.webp');" class="img-fluid" alt="Mr. Arjun Paramar">
            </span>
        </div>
    </div>
</div>
<div class="testimonial-one">
    <div class="img">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
        <h3><span>Quality products at great pricing!!</span></h3>
        <p>Pricing and Products quality of Auretics Products is at its best. I should have joined this amazing opportunity earlier that could have brightened my future earlier. Its an amazing platform to be the king of your own business.</p>
        <div class="author">
            <p>
                <span>Mr. Dashrath Thakur</span>
                <br>
                <span>Business Owner, ABC Start-up</span>
            </p>
            <span>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/test-9.webp');" class="img-fluid" alt="Mr. Dashrath Thakur">
            </span>
        </div>
    </div>
</div>