<?php
/*
 Template Name: Testimonial Page Template
  */
get_header();
?>

<?php
// Banner Section
get_template_part('template-parts/banner-section');
?>

<?php if(have_rows('testimonial_reviews')): ?>
    <?php while(have_rows('testimonial_reviews')): the_row(); ?>
        <section class="testimonial-box">
            <div class="container">
                <div class="row">
                    <div class="col-12 col--md-12 col-sm-12 col-lg-12">
                        <div class="testimonial-slider">
                        <?php if(have_rows('testimonial')): ?>
                            <?php while(have_rows('testimonial')): the_row(); ?>
                                <div class="testimonial-one">
                                    <div class="img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon/arrow.svg');" alt="arrow">
                                        <h3><?php echo get_sub_field('comment_heading'); ?></h3>
                                        <p class="join-para"><?php echo get_sub_field('content'); ?></p>
                                        <div class="author">
                                            <p>
                                                <span><?php echo get_sub_field('reviewer_name'); ?></span>
                                                <br>
                                                <span><?php echo get_sub_field('reviewer_title'); ?></span>
                                            </p>
                                            <span>
                                                <img src="<?php echo get_sub_field('reviewer_image'); ?>" class="img-fluid" alt="<?php echo get_sub_field('reviewer_name'); ?>">
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                            
                        </div>
                    </div>

                </div>
            </div>

        </section>
    <?php endwhile; ?>
<?php endif; ?>








<?php
// Banner Section
get_template_part('template-parts/join-today-section');
?>




<?php
get_footer();
?>